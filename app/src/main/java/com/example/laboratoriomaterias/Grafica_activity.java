package com.example.laboratoriomaterias;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Map;

public class Grafica_activity extends AppCompatActivity {
    private  float[][] datosxy;
    private String[] datosCrudos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grafica_activity);
        Intent intent = getIntent();
        String datos;
        datos= intent.getStringExtra(Laboratorio_fisica.MIS_DATOS);
        datosCrudos=datos.split("|");
        datosxy= new float[datosCrudos.length][2];
        String []separados = new String[2];
        for (int i =0; i< datosCrudos.length;i++){
            separados = datosCrudos[i].split(",");
            datosxy[i][0]= Float.parseFloat(separados[0]);
            datosxy[i][1]= Float.parseFloat(separados[1]);
        }



    }
    public void graficarDatos(){

        LineChart line = findViewById(R.id.grafica);
        line.setScrollX(2);
        line.setBorderColor(0);
        line.setBackgroundColor(1);
        ArrayList<Entry> valores = new ArrayList<>();
        for (int i = 0; i < datosCrudos.length; i++) {
            valores.add(new Entry(datosxy[i][0],datosxy[i][1]));
        }
        LineDataSet set1 = new LineDataSet(valores,"Probando");
        LineData dat = new LineData(set1);
        line.setData(dat);
    }
}
